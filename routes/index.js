const express = require('express');
const router = express.Router();

const { authentication } = require('../middlewares/authentication/index')

const { registerValidator } = require('../middlewares/Validator/registerValidator');
const { updateUserValidator } = require('../middlewares/Validator/updateUserValidator')
const { loginValidator } = require('../middlewares/Validator/loginValidator');
const { updatePassValidator } = require('../middlewares/Validator/passwordValidator')
const { createAndUpdateRoomValidator } = require('../middlewares/Validator/roomClassValidator');

const {
    createUser,
    AllUser,
    detailUser,
    updateUser,
    updatePassword,
    deleteUser,
    loginUser
} = require('../controllers/users');
const {
    getAllChosenclass,
    getDetailChosenclass,
    createChosenclass,
    updateChosenclass,
    deleteChosenclass,
    leaveClass
} = require('../controllers/chosenclass');
const {
    getAllTopics,
    getDetailTopic,
    createTopic,
    updateTopic,
    deleteTopic
} = require('../controllers/topics');
const {
    createRoomClass,
    allRoomClass,
    allMyClass,
    detailRoomClass,
    updateRoomClass,
    removeRoomClass
} = require('../controllers/roomClasses');

const { getHistory } = require('../controllers/histories')
const { getUserParticipant, approveParticipant } = require('../controllers/participants')
const { sendEmailVerification } = require('../controllers/verifyEmail')

// integrated jwt jitsi
const { tokenJitsi } = require('../controllers/jwtJitsi')

router.post('/token-generator/token', tokenJitsi)

router.get('/user/verify', sendEmailVerification)

// User
router.post('/user/register', registerValidator, createUser);
router.post('/user/login', loginValidator, loginUser);
router.get('/user/profile', authentication, detailUser);
router.put('/user/profile', authentication, updateUserValidator, updateUser);
router.patch('/user/password', authentication, updatePassValidator, updatePassword);
router.delete('/user', authentication, deleteUser);

// history
router.get('/user/history', authentication, getHistory);

// topics
router.get('/topics', getAllTopics);
router.get('/topics/:id', getDetailTopic);
router.post('/topics', authentication, createTopic);
router.put('/topics/:id', authentication, updateTopic);
router.delete('/topics/:id', authentication, deleteTopic);

// room class
router.get('/studyroom', allRoomClass);
router.post('/studyroom', authentication, createAndUpdateRoomValidator, createRoomClass);
router.get('/studyroom/:id', authentication, detailRoomClass);

// get participant in restricted class
router.post('/participant', authentication, approveParticipant);
// select participant that user wants to approve
router.get('/participant/:id', authentication, getUserParticipant);

// select / chosen class
router.post('/chosenclass', authentication, createChosenclass);

// leave class
router.delete('/leaveclass', authentication, leaveClass);

// my class
router.get('/myclass/createdclass', authentication, allMyClass);
router.post('/myclass/createdclass', authentication, createAndUpdateRoomValidator, createRoomClass);
router.get('/myclass/createdclass/:id', authentication, detailRoomClass);
router.put('/myclass/createdclass/:id', authentication, createAndUpdateRoomValidator, updateRoomClass);
router.delete('/myclass/createdclass/:id', authentication, removeRoomClass);

// joined class
router.get('/myclass/joinedclass', authentication, getAllChosenclass);
router.get('/myclass/joinedclass/:id', authentication, getDetailChosenclass);
router.post('/myclass/joinedclass', authentication, createAndUpdateRoomValidator, createRoomClass);
router.put('/myclass/joinedclass/:id', authentication, updateChosenclass);
router.delete('/myclass/joinedclass/:id', authentication, deleteChosenclass);

module.exports = router;