List of endpoint:

<!-- user -->
post    /user/register             => daftar akun/user
post    /user/login                => login masuk kedalam apps
get     /user                      => mendapatkan data user/profile yang sedang login
get     /user/history              => mendapatkan history user berdasarkan user telah join kedalam kelas mana saja
put     /user                      => update data user/profile kecuali password
patch   /user                      => update password user/profile

<!-- studyroom -->
get     /studyroom                 => mendapatkan semua data room yang telah dibuat semua user
get     /studyroom/:id             => mendapatkan detail data pada 1 class/room berdasarkan ID yang dibuat semua user
post    /studyroom                 => membuat class/room baru di endpoint /studyroom

<!-- created class -->
get     /myclass/createdclass      => mendapatkan semua data class/room yang dibuat oleh user A, user A tidak bisa mendapatkan data class/room user B
get     /myclass/createdclass/:id  => mendapatkan detail data pada 1 class/room yang hanya dibuat user A
post    /myclass/createdclass      => membuat class/room baru  di endpoint /myclass/createdclass
put     /myclass/createdclass/:id  => update data class/room, contoh: user A hanya bisa mengupdate class/room yang dibuat oleh user A dan user A tidak bisa update class/room yang dibuat oleh user B
delete  /myclass/createdclass/:id  => delete data class/room, contoh: user A hanya bisa delete class/room yang dibuat oleh user A dan user A tidak bisa delete class/room yang dibuat oleh user B

<!-- joined class -->
get     /myclass/joinedclass       => mendapatkan semua data class/room berdasarkan user yang telah join dikelas mana saja
get     /myclass/joinedclass/:id   => mendapatkan detail data pada 1 class/room berdasarkan ID
post    /myclass/joinedclass       => membuat class/room baru di endpoint /myclass/joinedlcass

<!-- chosen class -->
post    /chosenclass               => user memilih class/room yang ingin diikuti berdasarkan id_room_class
