require("dotenv").config();
const express = require("express");
const fileUpload = require("express-fileupload");
const cors = require("cors");
const cloudinary = require("cloudinary").v2;

const router = require("./routes");
const errorHandler = require('./middlewares/Errorhandler/ErrorHandler');

const app = express();
const port = process.env.PORT || 3000;

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());
app.use(express.static('public'));

app.use(router);

app.get('*', (req, res, next) => {
  res.send("404 Page Not Found");
});

app.use(errorHandler);

app.listen(port, () => console.log(`Server running on ${port}`));

module.exports = app;
