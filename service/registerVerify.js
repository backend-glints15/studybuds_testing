let nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'studybuds.project@gmail.com',
        pass: 'semogaselesai'
    }
});

const template = ({ name, link }) =>
    `
<b>Welcome ${name} to StudyBuds!</b>

<p> Please click this link to verify your StudyBuds account ${link}</p>
`

exports.sendMail = ({ recipient, verificationLink }) => {
    try {
        console.log(`sending email to ${recipient}`)
        const nameFromEmail = recipient.split('@')[0]
        const mailOptions = {
            from: 'studybuds.project@gmail.com',
            to: recipient,
            subject: 'StudyBuds: Verification Email',
            html: template({ name: nameFromEmail, link: verificationLink }),
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });
    }
    catch (err) {
        throw err
    }
}