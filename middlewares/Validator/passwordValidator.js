const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");
const cloudinary = require("cloudinary").v2;

exports.updatePassValidator = async (req, res, next) => {
    try {
        const errors = [];

        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\.\,])(?=.{8,12})/.test(req.body.password)) {
            errors.push("Please fill in the password with a length of 8-12 characters, consisting of a combination of uppercase, lowercase, symbol, and number");
        }

        if (errors.length > 0) {
            return res.status(400).json({ status: 400, success: false, errors: errors })
        }

        next();

    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, errors: 'Internal server error update password validator', error })
    }
}