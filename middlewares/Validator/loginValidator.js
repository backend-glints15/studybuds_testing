const validator = require("validator");

exports.loginValidator = async (req, res, next) => {
  try {
    const errors = [];

    if (validator.isEmpty(req.body.email, { ignore_whitespace: false })) {
      errors.push("Please fill your email");
    }

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email)) {
      errors.push("Please enter the correct Email address");
    }

    if (validator.isEmpty(req.body.password, { ignore_whitespace: false })) {
      errors.push("Please fill your password");
    }

    if (errors.length > 0) {
      return res.status(400).json({ success: false, errors: errors });
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, errors: ["Internal Server Error Login Validator"] });
  }
};