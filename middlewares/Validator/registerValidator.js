const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");
const cloudinary = require("cloudinary").v2;

exports.registerValidator = async (req, res, next) => {
  try {
    const errors = [];

    if (validator.isEmpty(req.body.fullname, { ignore_whitespace: false })) {
      errors.push("Fullname must be filled");
    }

    if (req.body.fullname.length < 5) {
      errors.push("Fullname minimum 5 characters");
    }

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email)) {
      errors.push("Please enter the correct Email address");
    }

    if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\.\,])(?=.{8,12})/.test(req.body.password)) {
      errors.push("Please fill in the password with a length of 8-12 characters, consisting of a combination of uppercase letters, lowercase letters, and number");
    }

    // if (!validator.isStrongPassword(req.body.confirmPassword, [{ minLength: 8, minLowercase: 1, minUppercase: 1, minNumbers: 1, }])) {
    //   errors.push("Confirmation Password => Please fill in the password with a length of 8-12 characters, consisting of a combination of uppercase letters, lowercase letters, and numbers");
    // }

    // if (!validator.equals(req.body.password, req.body.confirmPassword)) {
    //   errors.push("Password and Password Confirmation is not match");
    // }

    if (errors.length > 0) {
      return res.status(400).json({ success: false, errors: errors });
    }

    if (req.files != null) {
      const file = req.files.imageUser;

      if (!file.mimetype.startsWith('image')) {
        errors.push('File must be an image');
      }

      if (file.size > 3000000) {
        errors.push('image must be less than 3MB');
      }

      if (errors.length > 0) {
        return res.status(400).json({ errors: errors })
      }

      let fileName = crypto.randomBytes(16).toString('hex');

      file.name = `${fileName}${path.parse(file.name).ext}`;

      const move = promisify(file.mv);

      await move(`./public/images/profiles/${file.name}`);

      const profileUser = await cloudinary.uploader.upload(`./public/images/profiles/${file.name}`)
        .then((result) => {
          return result.secure_url
        })

      req.body.imageUser = profileUser;

    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, errors: ["Internal Server Error Register And Update Validator"], error });
  }
};
