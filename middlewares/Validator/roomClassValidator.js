const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");
const cloudinary = require("cloudinary").v2;

exports.createAndUpdateRoomValidator = async (req, res, next) => {
    try {
        const errors = [];

        if (validator.isEmpty(req.body.id_topic, { ignore_whitespace: false })) {
            errors.push('Topic must be filled')
        }
        // if (validator.isEmpty(req.body.imageClass, { ignore_whitespace: false })) {
        //     errors.push('Image must be filled')
        // }
        if (validator.isEmpty(req.body.title, { ignore_whitespace: false })) {
            errors.push('Title must be filled')
        }
        if (validator.isEmpty(req.body.limitParticipant, { ignore_whitespace: false })) {
            errors.push('Participant must be filled')
        }
        if (validator.isEmpty(req.body.date, { ignore_whitespace: false })) {
            errors.push('Date must be filled');
        }
        if (validator.isEmpty(req.body.startTime, { ignore_whitespace: false })) {
            errors.push('Time must be filled')
        }
        if (validator.isEmpty(req.body.endStart, { ignore_whitespace: false })) {
            errors.push('Time must be filled')
        }
        if (validator.isEmpty(req.body.description, { ignore_whitespace: false })) {
            errors.push('Description must be filled')
        }
        if (validator.isEmpty(req.body.roomStatus, { ignore_whitespace: false })) {
            errors.push('Room status must be filled')
        }

        if (errors.length > 0) {
            return res.status(400).json({ success: false, errors: errors })
        }

        if (req.files != null) {
            const file = req.files.imageClass;

            if (!file.mimetype.startsWith('image')) {
                errors.push('File must be an image');
            }

            if (file.size > 3000000) {
                errors.push('image must be less than 3MB')
            }

            if (errors.length > 0) {
                return res.status(400).json({ errors: errors })
            }

            let fileName = crypto.randomBytes(16).toString('hex')
            file.name = `${fileName}${path.parse(file.name).ext}`;
            const move = promisify(file.mv);
            await move(`./public/images/rooms/${file.name}`)

            const roomPicture = await cloudinary.uploader.upload(`./public/images/rooms/${file.name}`)
                .then((result) => {
                    return result.secure_url
                })
            req.body.imageClass = roomPicture
        }

        next();

    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, errors: 'Internal Server Error Create and Update Room Validator' })
    }
}
