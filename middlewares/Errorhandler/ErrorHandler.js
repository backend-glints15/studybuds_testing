module.exports = (err, req, res, next) => {
    // let errors = new Error();
    // errors.status = 404;
    // next(err)

    if (err.status) {
        console.log(err, '<<<<<<<<<<<<<<<<<<<')
        res.status(err.status).json({
            msg: err.msg
        })
    }

    let error;
    let errorStatus = 500;
    const errorMessages = [];
    if (err.name === "SequelizeValidationError") {
        error = "BAD_REQUEST";
        errorStatus = 400;
        err.errors.map((error, index) => {
            errorMessages.push(error.message);
        });
    } else if (err.name == 'SequelizeUniqueConstraintError') {
        error = "BAD_REQUEST";
        errorStatus = 400;
        err.errors.map((error, index) => {
            errorMessages.push(error.message);
        });
    } else {
        errorMessages.push("Something went wrong");
    }

    res.status(errorStatus).json({
        error,
        errorMessages,
    });

};
