'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fullname: {
        allowNull: true,
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING,
        unique: true,
        validate: {
          isEmail: true,
        }
      },
      password: {
        allowNull: true,
        type: Sequelize.STRING,
        validate: {
          len: [8, 12]
        }
      },
      imageUser: {
        allowNull: true,
        type: Sequelize.STRING(1000)
      },
      bio: {
        allowNull: true,
        type: Sequelize.STRING(2000)
      },
      token: {
        allowNull: true,
        type: Sequelize.STRING(2000)
      },
      isVerified: {
        type: Sequelize.BOOLEAN
      },
      verificationCode: {
        type: Sequelize.STRING(2000)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};