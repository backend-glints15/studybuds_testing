const { Topic } = require("../models");

class Topics {
  // Get All Topics
  static async getAllTopics(req, res, next) {
    try {
      let data = await Topic.findAll({

      });
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Topic are not found!"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error)
      res.status(500).json({ errors: ["Internal Server Error Get All Topics"] });

    }
  }

  // Get Detail Topic
  static async getDetailTopic(req, res, next) {
    try {
      let data = await Topic.findOne({
        where: { id: req.params.id },
      });

      // If topic not exists
      if (!data) {
        return res.status(404).json({ errors: ["Topic not found"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error Get Detail Topic"] });
    }
  }

  // Create Topic
  static async createTopic(req, res, next) {
    try {
      const createTopic = await Topic.create(req.body);

      const data = await Topic.findOne({
        where: {
          id: createTopic.id,
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error Create Topic"] });
    }
  }
  static async updateTopic(req, res, next) {
    try {
      const updateTopic = await Topic.update(req.body, {
        where: {
          id: req.params.id,
        },
      });
      if (updateTopic[0] === 0) {
        return res.status(404).json({ errors: ["Topic not found"] });
      }
      // Find Updated Topic
      const data = await Topic.findOne({
        where: {
          id: req.params.id,
        },
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error Update Topic"] });
    }
  }

  static async deleteTopic(req, res, next) {
    try {
      let data = await Topic.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({ errors: ["Topic not found!"] });
      }
      res.status(200).json({ message: " The topic was deleted!" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error Delete Topic"] });
    }
  }
}
module.exports = Topics;