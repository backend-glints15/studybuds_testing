const { User, RoomClass, History } = require("../models");
const moment = require('moment');

class HistoryController {
    static async getHistory(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            req.body.id_user = currentUser.id;

            if (currentUser === null) {
                return res.status(404).json({ errors: 'You must Login first' })
            }

            let data = await History.findAll({
                where: {
                    id_user: currentUser.id
                },
                attributes: { exclude: ['id_user', 'deletedAt'] },
                include: [
                    {
                        model: User,
                        attributes: ['fullname', 'email'],
                    },
                    {
                        model: RoomClass,
                        attributes: ['title', 'roomStatus'],
                    }
                ],
                order: [['id', 'DESC']]
            })

            for (let i = 0; i < data.length; i++) {
                const getCreatedAt = data[i].dataValues.createdAt;
                const formatDate = new Date(getCreatedAt).toLocaleString();
                const parseTime = moment(formatDate, 'MM/DD/YYYY, h:mm:ss A').fromNow()
                data[i].dataValues.dateTime = parseTime;
            }

            if (data.length === 0) {
                return res.status(404).json({ errors: ["history are not found!"] });
            }

            res.status(200).json({ data })
        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal server error get history' })
        }

    }
}

module.exports = HistoryController;