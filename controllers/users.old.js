const { user } = require("../models");
const { createToken, encrypt, decrypt } = require('../helpers/index');

class User {

  static async getAllUser(req, res, next) {
    try {
      const data = await user.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt", "token"],
        },
      });

      res.status(200).json({ success: true, data: data });

    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error Get All User"] });
    }
  }

  static async getDetailUser(req, res, next) {
    try {
      const userId = req.userData.id

      const data = await user.findOne({
        where: { id: +userId },
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt", "token"],
        },
      });

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error Get Detail User"] });
    }
  }

  static async createUser(req, res, next) {
    try {
      const { fullname, email, password } = req.body
      const hashPassword = encrypt(password)

      const checkUser = await user.findOne({
        where: {
          email: email,
        }
      })

      if (checkUser != null) {
        return res.status(500).json({ success: false, errors: ["Cannot register, email was registered"] });
      }

      if (checkUser == null) {

        const newUser = await user.create({
          fullname,
          email,
          password: hashPassword,
        })

        const data = await user.findOne({
          where: {
            email: email,
          },
          attributes: {
            exclude: ["createdAt", "updatedAt", "deletedAt", "token"],
          }
        })

        res.status(200).json({ success: true, data: data });
      }

    } catch (error) {
      console.log(error);
      res.status(500).json({ success: false, errors: ["Internal Server Error Create User"] });
    }
  }

  static async updateUser(req, res, next) {
    try {
      const userId = req.userData.id
      const { fullname, email, password } = req.body
      const hashPassword = encrypt(password)

      const updateData = await user.update({
        fullname,
        email,
        password: hashPassword,
      },
        {
          where: { id: +userId },
        });

      const data = await user.findOne({
        where: {
          id: +userId,
        },
      });

      res.status(201).json({ success: true, message: ["Success Update Data"] });

    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error Update User"] });
    }
  }

  // static async updateUser(req, res, next) {
  //   try {
  //     const userId = req.userData.id
  //     const { fullname, email, password } = req.body
  //     const hashPassword = encrypt(password)

  //     const updateData = await user.update({
  //       fullname,
  //       email,
  //       password: hashPassword,
  //     },
  //       {
  //         where: { id: +userId },
  //       });

  //     const data = await user.findOne({
  //       where: {
  //         id: +userId,
  //       },
  //     });

  //     res.status(201).json({ success: true, message: ["Success Update Data"] });

  //   } catch (error) {
  //     res.status(500).json({ success: false, errors: ["Internal Server Error"] });
  //   }
  // }

  static async deleteUser(req, res, next) {
    try {

      const userId = req.userData.id
      const deletedData = await user.destroy({
        where: {
          id: +userId,
        },
        force: true
      });

      if (deletedData.id !== +userId) {
        return res
          .status(404)
          .json({ success: false, message: ["User has been deleted"] });
      }

      res.status(200).json({ success: true, message: ["Success deleting data"] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  static async login(req, res, next) {
    try {
      const email = req.body.email
      const password = req.body.password

      const loginUser = await user.findOne({
        where: {
          email: email
        }
      })

      if (loginUser == null) {
        return res.status(401).json({
          success: false,
          errors: "Please Input the correct email and password"
        })
      }

      const hashPass = loginUser.dataValues.password
      const compareResult = decrypt(password, hashPass)

      if (!compareResult) {
        return res.status(401).json({
          success: false,
          errors: "Please Input the correct email and password"
        })
      }

      const payload = loginUser.dataValues
      const token = createToken(payload)
      res.status(200).json({
        success: true,
        token: token,

      })
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }
}

module.exports = User;
