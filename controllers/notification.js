const { Notification, User } = require("../models");

class Notifications{
    // Create Notification
    async createNotification(req, res, next){
        try {
            res.status(200).jsom({data});
        } catch (error) {
            res.status(500).json({ errors: ["Internal Server Error"]});
        }
    }
    // Get All Notifications
    async getAllNotification(req, res, next){
        try {
            let data = await Notification.findAll
            res.status(200).json({data});
        } catch (error) {
            res.status(500).json({ errors: ["Internal Server Error"]});
        }
    }
    // Get Detail Notification
    async getDetailNotification(req, res, next){
        try {
            res.status(200).jsom({data});
        } catch (error) {
            res.status(500).json({ errors: ["Internal Server Error"]});
        }
    }
    // Update Notification
    async updateNotification(req, res, next){
        try {
            res.status(200).jsom({data});
        } catch (error) {
            res.status(500).json({ errors: ["Internal Server Error"]});
        }
    }
    // Delete Notification
    async deleteNotification(req, res, next){
        try {
            let data = await Notification.destroy({ where: {id: req.params.id}});

            if (!data){
                return res.status(404).json({ errors: ["Notification not found!"]})
            }
            res.status(200).jsom({ message: "The notification was deleted"});
        } catch (error) {
            res.status(500).json({ errors: ["Internal Server Error"]});
        }
    }
}
module.exports = new Notifications();