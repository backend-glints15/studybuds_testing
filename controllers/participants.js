const { ChosenClass, User, RoomClass, History, Topic } = require("../models");
const { Op } = require('sequelize');

class RegisteredUser {
    static async getUserParticipant(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            req.body.id_user = currentUser.id;

            if (currentUser === null) {
                return res.status(404).json({ errors: 'You must Login first' })
            }

            let data = await ChosenClass.findAll({
                where: {
                    id_room_class: req.params.id
                }
            })

            console.log(data, "<<<<<<<<DATA");
            console.log(req.params, "<<<<<<PARAMS");

            res.status(200).json({ data });
        } catch (error) {
            console.log(error);
        }
    }

    static async approveParticipant(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            req.body.id_user = currentUser.id;

            if (currentUser === null) {
                return res.status(403).json({ message: 'You must Login first' })
            }

            let dataRoomClass = await RoomClass.findOne({
                where: { id: req.body.id_room_class }
            })

            if (dataRoomClass.roomStatus == "Restricted") {
                const setApprove = await ChosenClass.update(
                    { isApproved: true },
                    {
                        where: {
                            [Op.and]: [{ id_user: req.body.arrIdUser, id_room_class: req.body.id_room_class }]
                        }
                    }
                )
                await RoomClass.update({ countJoined: +dataRoomClass.dataValues.countJoined + 1 }, { where: { id: req.body.id_room_class } })

                res.status(201).json({ setApprove })
            }


        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = RegisteredUser;