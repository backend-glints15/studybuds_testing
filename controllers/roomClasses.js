const { RoomClass, Topic, User, sequelize } = require('../models');
const { Op } = require('sequelize');
const moment = require('moment');

class RoomClassController {
    static async createRoomClass(req, res, next) {
        try {

            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            req.body.id_user = currentUser.id;

            // generate unique room name
            let random1 = (Math.random() + 1).toString(36).substring(7);
            let random2 = (Math.random() + 1).toString(36).substring(7);
            let joinTitle = req.body.title.split(' ').join('-').toLowerCase();
            let roomName = `${joinTitle}-${random1}-${random2}`

            const getDate = req.body.date;
            const getStartTime = req.body.startTime;
            const getEndTime = req.body.endStart;

            const newData = await RoomClass.create({
                id_user: req.body.id_user,
                id_topic: req.body.id_topic,
                imageClass: req.body.imageClass,
                title: req.body.title,
                countJoined: 0,
                limitParticipant: req.body.limitParticipant,
                date: moment(getDate).format('ddd, D MMMM YYYY'),
                startTime: moment(`${getDate} ${getStartTime}`).format('HH:mm'),
                endStart: moment(`${getDate} ${getEndTime}`).format('HH:mm'),
                // time: `${date}:req.body.startTime`
                description: req.body.description,
                roomStatus: req.body.roomStatus,
                roomName: roomName,
            });

            let data = await RoomClass.findOne({
                where: {
                    id: newData.id,
                },
                attributes: { exclude: ['id_user', 'id_topic', 'deletedAt'] },
                include: [
                    {
                        model: User,
                        attributes: { exclude: ['password', 'token', 'deletedAt'] }
                    },
                    {
                        model: Topic,
                        attributes: { exclude: ['deletedAt'] }
                    }
                ],
            });



            res.status(201).json({ status: 201, success: true, data });
        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal server error create class', message: error });
        }
    }

    static async allRoomClass(req, res, next) {
        try {

            let {
                page = 1,
                limit = 8,
                search,
                status,
                top,
                orders = 'id',
                sort = 'DESC'
            } = req.query;

            // console.log(search);
            page = parseInt(page);
            limit = parseInt(limit);

            // const filterSearch = search ? { [Op.like]: `%${search}%` } : { [Op.ne]: null };
            // const roomStatus = status ? status : { [Op.ne]: null };
            // const topics = top ? top : { [Op.ne]: null };

            // is there any filter (at least 1) ??
            const hasFilter = search || status || top
            const where = {}

            if (hasFilter) {
                if (filterSearch) where['title'] = { [Op.like]: `%${search}%` }
                if (roomStatus) where['roomStatus'] = status
                if (top) where['topics'] = top
            }

            let data = await RoomClass.findAndCountAll({
                ...{ where },
                attributes: { exclude: ['id_user', 'id_topic', 'deletedAt'] },
                include: [
                    {
                        model: User,
                        attributes: { exclude: ['password', 'token', 'deletedAt'] }
                    },
                    {
                        model: Topic,
                        attributes: { exclude: ['deletedAt'] }
                    }
                ],
                order: [[orders || 'createdAt', sort || 'ASC']],
                // order: [['id', 'DESC']],
                limit: limit,
                offset: (page - 1) * limit
            });

            // return console.log(data.count);

            if (data.length === 0) {
                return res.status(404).json({ errors: 'Class not found' });
            }
            // console.log(page, 'current page');
            let nextPage = page + 1;
            // console.log(nextPage, "next page");
            let lastPage = Math.round(data.count / limit)
            // console.log(lastPage, "last page");
            if (nextPage >= lastPage) {
                nextPage = lastPage
            }

            res.status(200).json({ status: 200, success: true, currentPage: page, nextPage, dataPerPage: data.rows.length, totalData: data.count, data })

        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal server error all class' });
        }
    }

    static async allMyClass(req, res, next) {
        try {

            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            req.body.id_user = currentUser.id;

            let {
                page = 1,
                limit = 8,
                search,
                status,
                top,
                orders = 'id',
                sort = 'DESC'
            } = req.query

            console.log(search);
            page = parseInt(page)
            limit = parseInt(limit)

            const filterSearch = search ? { [Op.like]: `%${search}%` } : { [Op.ne]: null };
            const roomStatus = status ? status : { [Op.ne]: null };
            const topics = top ? top : { [Op.ne]: null };

            let data = await RoomClass.findAndCountAll({
                where: {
                    id_user: currentUser.id,
                    title: filterSearch,
                    roomStatus,
                    id_topic: topics
                },
                attributes: { exclude: ['id_user', 'id_topic', 'deletedAt'] },
                include: [
                    {
                        model: User,
                        attributes: { exclude: ['password', 'token', 'deletedAt'] }
                    },
                    {
                        model: Topic,
                        attributes: { exclude: ['deletedAt'] }
                    }
                ],
                order: [[orders || 'createdAt', sort || 'DESC']],
                // order: [['id', 'DESC']],
                limit: limit,
                offset: (page - 1) * limit
            });

            if (data.length === 0) {
                return res.status(404).json({ errors: 'Class not found' });
            }
            let nextPage = page + 1;
            let lastPage = Math.round(data.count / limit)
            if (nextPage >= lastPage) {
                nextPage = lastPage
            }

            res.status(200).json({ status: 200, success: true, currentPage: page, nextPage, dataPerPage: data.rows.length, totalData: data.count, data })

        } catch (error) {
            res.status(500).json({ error: 'Internal server error my class', message: error });
        }
    }

    static async detailRoomClass(req, res, next) {
        try {

            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            req.body.id_user = currentUser.id;

            let data = await RoomClass.findOne({
                where: {
                    id: req.params.id
                },
                attributes: { exclude: ['id_user', 'id_topic', 'deletedAt'] },
                include: [
                    {
                        model: User,
                        attributes: { exclude: ['password', 'token', 'deletedAt'] }
                    },
                    {
                        model: Topic,
                        attributes: { exclude: ['deletedAt'] }
                    }
                ],
            });

            if (!data) {
                return res.status(404).json({ errors: 'Class not found' });
            }

            res.status(200).json({ status: 200, success: true, data });

        } catch (error) {
            res.status(500).json({ errors: 'Internal server error detail class', message: error });
        }
    }

    static async updateRoomClass(req, res, next) {
        try {

            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            const currentRoom = await RoomClass.findOne({
                where: {
                    id: req.params.id
                }
            });

            if (currentRoom == null) {
                return res.status(404).json({ error: 'Class not found' });
            }

            if (currentUser.id != currentRoom.id_user) {
                return res.status(403).json({ error: 'Access denied to edit this class' });
            }

            if (currentUser == null) {
                return res.status(403).json({ error: 'Access denied to edit this class' })
            }

            await RoomClass.update(req.body, {
                where: {
                    id: req.params.id,
                },
            });

            const data = await RoomClass.findOne({
                where: {
                    id: req.params.id,
                },
                attributes: { exclude: ['id_user', 'id_topic', 'deletedAt'] },
                include: [
                    {
                        model: User,
                        attributes: { exclude: ['password', 'token', 'deletedAt'] }
                    },
                    {
                        model: Topic,
                        attributes: { exclude: ['deletedAt'] }
                    }
                ],
            });

            res.status(201).json({ status: 201, success: true, data });

        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal server error update class', message: error });
        }
    }

    static async removeRoomClass(req, res, next) {
        try {

            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            const currentRoom = await RoomClass.findOne({
                where: { id: req.params.id }
            })

            if (currentRoom === null) {
                return res.status(404).json({ error: 'Class not found' })
            }

            if (currentUser.id != currentRoom.id_user) {
                return res.status(403).json({ error: 'Access denied to delete this class' })
            }

            if (currentUser == null) {
                return res.status(403).json({ error: 'Access denied to delete this class' })
            }

            await RoomClass.destroy({
                where: {
                    id: req.params.id
                }
            });

            res.status(200).json({ status: 200, success: true, message: 'Success delete class' });
        } catch (error) {
            res.status(500).json({ errors: 'Internal server error delete class' });
        }
    }
}

module.exports = RoomClassController;