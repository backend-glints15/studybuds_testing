let jsonwebtoken = require('jsonwebtoken');
let uuid = require('uuid-random');

class generateTokenJitsi {
    static async tokenJitsi(req, res, next) {
        try {
            /**
         * Function generates a JaaS JWT.
         */
            let { name, email, avatar } = req.body
            // console.log(req.body);

            const generate = (privateKey, { id, name, email, avatar, appId, kid }) => {
                const now = new Date()
                const jwt = jsonwebtoken.sign({
                    aud: 'jitsi',
                    context: {
                        user: {
                            id,
                            name,   // dinamis
                            avatar,
                            email: email,   //dinamis
                            moderator: 'true'   //dinamis
                        },
                        features: {
                            livestreaming: 'true',
                            recording: 'true',
                            transcription: 'true',
                            "outbound-call": 'true'
                        }
                    },
                    iss: 'chat',
                    room: '*',      //dinamis
                    sub: appId,
                    exp: Math.round(now.setHours(now.getHours() + 3) / 1000),   //dinamis
                    nbf: (Math.round((new Date).getTime() / 1000) - 10)         //dinamis
                }, privateKey, { algorithm: 'RS256', header: { kid } })
                return jwt;
            }

            /**
             * Generate a new JWT.
             */
            const token = generate(`-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCvh7H2JSCFdyhN
Pa2fOggN9gNoWr1brUqQ0Nq4KeoJHDU+c2HRV+8tHAWvPdWcf+XE2IlxSlAXafvs
eQxptlC5BFF6XYIT//JyJs9SZjP+DWv8wknU6dnl8CPg9L/lx8QaqlmV5ZKLHZdO
XcVVTG7PBOPkozCtwD+Q6fayrZ3onfZp9LLZaqclLivO+u/kwhrHF4ZT9BQugSRg
nzRxy0gfzN7HPgALYvWv83oWXWIH9RcZrtkELAfCKKfF1cbSrdLp54L+MgbEGXg+
JPZ4r9CFLSbzPXexw+QLrVViiKoGMYxFQxq7w73CtRx+wVQOH/F7yJlyomTsEsao
Ah9C2qgrAgMBAAECggEALCcA376FCK1paMx78NJjFkfsoYI5oWbcm8K9UZfRc8sx
pWzWjRcGZZ1n3KW8JqqRPEuPfvzHWqxhHyFvuuOOOIvahuIghzFZZddXTQ3CRERS
BnlEoxbTpaAbFknaP01Zia+R6z+c9ftShPbXXVZlhRNg07Es3HfARY5kbJt5Xfp7
LeZuXNJbc+nUwnBTaswz38P38MfA1D64l/flTZg4CuGjdf2GhQi1vQBTZLwCG+rA
Xuye9gVObQD7fqbxbCEfaDs2XOVHBjjH8G8BHTvLTL9yyUWJf0Ywk+YEamWg1EKO
x0H3du37DNF+7MipJGi/2qcprx47LbIo4JCDUOCPMQKBgQDebCChvGl2GfZanyh6
YA6n26uVhtfW/Ix3ycUASpM5vNEFiQh/35UjVUrY1Wz8TIW1TKiJ7PxXHrYWz3Ud
HLhYANDQwszVON9JV0GjD1d9gSemamST0zAzkirMPDuPe0Ze+9UeDyj6EHWGVJi6
oXKVvidafyEL5/Idzz3fTNWpAwKBgQDKB1aeCZjweArxUAtEcacH1IyqVVKBHsTA
3Br04RybKG2IdyupqBjBAy5sh+bNf/nGP801qMDlsnk4d38lTp5iNqXR/NlWkssF
rxOg/fYnzguwog/OIaMculHgroH6MmlOFmeH+TAgEUduzyXiAdpfpdqy71hArtHu
y3unAdbXuQKBgQDeO3krQH668tvT1dxy0VEzZGf/Qbic5L9Q3LfKP7j0YnUSK8Xj
RjjUEd9z4WNAfxDFge+Px8qlHVlgVy4zxUNnIoGBZHGWxXlL5TP+wVBwZMCCJSGT
Fdq+XFgIslHsXYaFVuesXgdaO2HBsvra1n8PoOvfHtCc/cZLBItUdoewkwKBgH8s
5WqwsfTWLFVBxQX94V59f1sOQNSFMOgS5mk2eBJ3/ZOFKRgwd/ywav3pAvJj2DDj
IuPfaiZuFf7kwkYS2oMK0ZGBcLMdzu2yC1Ix7E2cWCGFsxtYc1x3MWmVkL6WE1IT
qnKe+5uEJQptHfTKo9PIDQbMwipdokuHDjzQXJUJAoGBAMr5QDXIGMLjxx7OZCtw
EsSUz1wZP0Y8R15BhZkrv/fEeW4o5/n+Gys9JSzorbbtOUCdNDzA6efYw+tPIgC7
5f+5Zn+rB8QidastS6Mzj+T/kaWKYd7UER4rZSIkuMmbKk+ZtD4UNEQX3jii93DU
zvzue1Zma9RK7QdkUip/rMYk
-----END PRIVATE KEY-----`, {
                id: uuid(),
                name: name,
                email: email,
                avatar: avatar,
                appId: "vpaas-magic-cookie-8f42984f70004c1cafb5c9fdc7b13679", // Your AppID ( previously tenant )
                kid: "vpaas-magic-cookie-8f42984f70004c1cafb5c9fdc7b13679/247232"
            });

            res.json({ token })
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = generateTokenJitsi