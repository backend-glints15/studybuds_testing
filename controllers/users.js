const { User, sequelize } = require('../models')
const { decrypt, encrypt, createToken, verifyToken } = require('../helpers/index')
const { sendMail: sendEmailRegistration } = require('../service/registerVerify')
class UserController {
    // creating user
    static async createUser(req, res, next) {
        let errorType;

        try {
            // find user
            const data = await User.findOne({
                where: {
                    email: req.body.email
                },
                attributes: { exclude: ["updatedAt", "deletedAt", "password", "token"] }
            })

            if (data != null) {
                // return jika email sudah terdaftar
                errorType = 1;
                res.status(400).json({ message: "Email already registered" });
            }

            let { fullname, email, password, imageUser, bio } = req.body;

            password = encrypt(password)

            const newData = await User.create({
                fullname, email, password, imageUser, bio,
            });

            let verifyUrl = `http://localhost:3000/user/verify`

            const payload = {
                fullname,
                email,
                password,
                imageUser,
                bio
            }

            const verifyToken = createToken(payload)
            verifyUrl += `?token=${verifyToken}`

            sendEmailRegistration({
                recipient: email, verificationLink: verifyUrl
            })
            // res.status(201).json({ data: req.body });

            if (!newData.isVerified) {
                return res.status(201).json({ message: 'Please check your email to verify account, Note: if inbox is not found, check spam folder' })
            }

            const resData = await User.findOne({
                where: {
                    id: newData.id
                },
                attributes: { exclude: ["updatedAt", "deletedAt", "password", "token"] }
            })


            res.status(201).json({ status: 201, message: 'Success create an account!', data: resData });

        } catch (error) {
            console.log(error, "register<<<<<<<<<<<<<<<<");
            // res.status(500).json({ errors: "Error Creating User", message: error });
            next(error)
        }
    }
    // show all user
    static async allUser(req, res, next) {
        try {
            const data = await User.findAll({
                // attributes: { exclude: ["createdAt", "updatedAt", "deletedAt", "token", "password"] }
            })

            if (data.length === 0) {
                return res.status(404).json({ status: 404, message: 'User not found!' })
            }

            res.status(200).json({ status: 200, success: true, message: 'success get all user', data });
        } catch (error) {
            res.status(500).json({ errors: 'Error getting all user', message: error });
        }
    }
    // show specific user
    static async detailUser(req, res, next) {
        try {

            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            if (currentUser === null) {
                return res.status(404).json({ errors: 'user not found' })
            }

            const data = await User.findOne({
                where: {
                    id: currentUser.id
                },
                attributes: { exclude: ["updatedAt", "deletedAt", "token", "password"] }
            });

            if (!data) {
                return res.status(404).json({ errors: "User not found" });
            }

            res.status(200).json({ status: 200, success: true, message: 'success get detail user', data });
        } catch (error) {
            res.status(500).json({ errors: 'Error getting all user', message: error });
        }
    }
    // updating user
    static async updateUser(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            if (currentUser === null) {
                return res.status(404).json({ errors: 'user not found' });
            }

            let { fullname, email, password, imageUser, bio } = req.body;

            // password = encrypt(password)
            await User.update(
                { fullname, email, password, imageUser, bio },
                {
                    where: { id: currentUser.id },
                });

            const data = await User.findOne({
                where: { id: currentUser.id },
                attributes: { exclude: ['password', 'token', 'updatedAt', 'deletedAt'] }
            });

            res.status(201).json({ status: 201, success: true, message: 'success updating user', data });
        } catch (error) {
            res.status(500).json({ errors: 'Error updating user', message: error });
        }
    }

    static async updatePassword(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            if (currentUser === null) {
                return res.status(404).json({ errors: 'user not found' });
            }

            if (!decrypt(req.body.currentPassword, currentUser.password)) {
                return res.status(404).json({ errors: 'Wrong current password' })
            }

            req.body.password = encrypt(req.body.password)

            await User.update(
                {
                    password: req.body.password
                },
                {
                    where: { id: currentUser.id },
                }
            );


            const data = await User.findOne({
                where: { id: currentUser.id },
                attributes: { exclude: ['password', 'token', 'deletedAt'] }
            })

            res.status(201).json({ status: 201, success: true, message: 'success update password', data })
        } catch (error) {
            res.status(500).json({ errors: 'Error updating password', message: error })
        }
    }

    // deleting user
    static async deleteUser(req, res, next) {
        try {
            const token = req.headers.authorization.replace('Bearer ', '');
            const currentUser = await User.findOne({
                where: { token }
            });

            if (currentUser == null) {
                return res.status(404).json({ errors: 'No delete access to this user!' });
            }

            const data = await User.destroy({
                where: { id: currentUser.id }
            })

            if (!data) {
                return res.status(404).json({ errors: 'User not found!' });
            }

            res.status(200).json({ status: 200, messages: 'Success delete!' });
        } catch (error) {
            res.status(500).json({ errors: 'Error deleting user', message: error });
        }
    }
    // =============================================================================================================
    // Login
    static async loginUser(req, res, next) {
        // untuk cek email di database
        const data = await User.findOne({
            where: { email: req.body.email },
        });

        if (data == null) {
            return res.status(404).json({ errors: 'Please input email or password correctly' })
        }

        if (!data.dataValues.isVerified) {
            return res.status(400).json({ errors: 'Please verify your account' })
        }

        // cek apa password memang dimiliki oleh email yang diinput
        let validPass = decrypt(req.body.password, data.password);
        if (!validPass) {
            return res.status(404).json({ errors: 'Please input your password correctly' })
        }

        const {
            fullname, email, imageUser, bio
        } = data.dataValues

        const tmpToken = {
            fullname, email, imageUser, bio
        }

        const token = createToken(tmpToken);
        await sequelize.query(`UPDATE Users SET token='${token}' WHERE id=${data.id}`);
        return res.status(200).json({ statusCode: 200, message: "Login success!", token });
    }

}

module.exports = UserController;