const { VerifyCode, User } = require('../models')
const { createToken, verifyToken } = require('../helpers/jwt')

class sendVerification {
    static async sendEmailVerification(req, res, next) {
        try {
            /**
       * now, email with verification url has already been sent to users.
       * they're about to click that link, and this controller will handle that
       * a complete url would look like this:
       * GET https://yourhost.herokuapp.com/verify?token=kjskjbdg.98hv9ubrg.o9ubwr9gub
       * 
       * as u can see, the url has a token with it, a token with user identifier encripted inside.
       */
            const { token } = req.query
            // const userData = jwt.verify(token, verificationSecret)
            const userData = verifyToken(token)
            /**
             * userData should contain payload that we've already assigned at registration. in this case, userData = {id} (check jwt sign at registration)
             * now that we have user identifier (id), we can do anything we want.
             */
            // console.log(userData);
            await User.update({ isVerified: true }, { where: { email: userData.email } })
            // do something else?

            res.status(200).json({
                success: true,
                message: 'Verification complete, Please login and enjoy!'
            })
        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false })
        }
    }
}

module.exports = sendVerification;