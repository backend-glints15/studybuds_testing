const { ChosenClass, User, RoomClass, History, Topic } = require("../models");
const { Op } = require('sequelize')
const moment = require('moment');

class ChosenClassController {
  // Get All 
  static async getAllChosenclass(req, res, next) {
    try {
      const token = req.headers.authorization.replace('Bearer ', '');
      const currentUser = await User.findOne({
        where: { token }
      });

      req.body.id_user = currentUser.id;

      if (currentUser === null) {
        return res.status(404).json({ errors: 'You must Login first' })
      }

      // find class untuk dapat jumlah participant

      let data = await ChosenClass.findAll({
        where: {
          id_user: currentUser.id
        },
        attributes: { exclude: ['id_user', 'id_room_class', 'updatedAt', 'deletedAt'] },
        include: [
          {
            model: User,
            attributes: { exclude: ['password', 'token', 'updatedAt', 'deletedAt'] }
          },
          {
            model: RoomClass,
            attributes: { exclude: ['id_user', 'id_topic', 'updatedAt', 'deletedAt'] },
            include: [
              {
                model: Topic,
                attributes: ['id', 'topic']
              }
            ]
          },
        ],
        order: [['id', 'desc']]
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["chosen class are not found!"] });
      }

      res.status(200).json({ data });
    } catch (error) {
      console.log(error)
      res.status(500).json({ errors: ["Internal Server Error get all"] });

    }
  }

  // Get Detail 
  static async getDetailChosenclass(req, res, next) {
    try {
      const token = req.headers.authorization.replace('Bearer ', '');
      const currentUser = await User.findOne({
        where: { token }
      });

      req.body.id_user = currentUser.id;

      let data = await ChosenClass.findOne({
        where: { id: req.params.id },

        attributes: { exclude: ['id_user', 'id_room_class', 'deletedAt'] },
        include: [
          {
            model: User,
            attributes: { exclude: ['password', 'token', 'deletedAt'] }
          },
          {
            model: RoomClass,
            attributes: { exclude: ['id_user', 'id_topic', 'updatedAt', 'deletedAt'] },
          },
        ],
      });

      if (!data) {
        return res.status(404).json({ message: "chosen class not found" });
      }

      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error get detail chosen class"] });
    }
  }

  // Create 
  static async createChosenclass(req, res, next) {
    try {
      const token = req.headers.authorization.replace('Bearer ', '');
      const currentUser = await User.findOne({
        where: { token }
      });

      req.body.id_user = currentUser.id;

      let checkUserAndRoom = await ChosenClass.findOne({
        where: {
          [Op.and]: [{ id_user: currentUser.id }, { id_room_class: req.body.id_room_class }]
        }
      })

      if (checkUserAndRoom) {
        return res.status(400).json({ message: 'You already join class, please leave first' })
      }

      // Mendapatkan value limitParticipant didalam tabel RoomClass
      let getParticipant = await RoomClass.findOne({
        where: {
          id: req.body.id_room_class,
        }
      })

      if (+getParticipant.dataValues.countJoined >= +getParticipant.dataValues.limitParticipant) {
        return res.status(400).json({ message: 'Sorry, maybe another time. Class already full' })
      }

      const createChosenclass = await ChosenClass.create(req.body);

      if (getParticipant.roomStatus == "Open") {
        await ChosenClass.update({ isApproved: true }, { where: { id_room_class: req.body.id_room_class } })
        await RoomClass.update({ countJoined: +getParticipant.dataValues.countJoined + 1 }, { where: { id: req.body.id_room_class } })
      }

      const data = await ChosenClass.findOne({
        where: {
          id: createChosenclass.id,
        },
        attributes: { exclude: ['id_user', 'id_room_class', 'updatedAt', 'deletedAt'] },
        include: [
          {
            model: User,
            attributes: ['id', 'fullname', 'email', 'createdAt']
          },
          {
            model: RoomClass,
            attributes: ['id', 'title', 'countJoined', 'limitParticipant', 'createdAt']
          },
        ],
      });

      const getTitle = data.dataValues.RoomClass.title;
      const getFullname = data.dataValues.User.fullname;
      // const createdTime = new Date(data.dataValues.createdAt).toLocaleString();
      // const parseTime = moment(createdTime, 'MM/DD/YYYY, h:mm:ss A').fromNow();

      await History.create({
        id_user: req.body.id_user,
        id_room_class: createChosenclass.id_room_class,
        history: `You have joined ${getTitle} by ${getFullname} class`,
      });

      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ errors: ["Internal Server Error Create chosen class"] });
    }
  }
  static async updateChosenclass(req, res, next) {
    try {

      const token = req.headers.authorization.replace('Bearer ', '');
      const currentUser = await User.findAll({
        where: { token }
      })

      const currentChosen = await ChosenClass.findOne({
        where: {
          id: req.params.id
        }
      })

      if (currentChosen == null) {
        return res.status(404).json({ error: 'chosen class not found' })
      }

      if (currentUser.id != currentChosen.id_user) {
        return res.status(403).json({ error: 'access denied to edit this chosen class' })
      }

      if (currentUser == null) {
        return res.status(403).json({ error: 'access denied to edit this chosen class' })
      }

      await ChosenClass.update(req.body, {
        where: {
          id: req.params.id,
        },
      });
      const data = await ChosenClass.findOne({
        where: {
          id: req.params.id,
        },
        include: [
          {
            model: User,
          },
          {
            model: RoomClass,
          }
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error Update "] });
    }
  }

  static async deleteChosenclass(req, res, next) {
    try {

      const token = req.headers.authorization.replace('Bearer ', '');
      const currentUser = await User.findOne({
        where: { token }
      });

      const currentChosen = await ChosenClass.findOne({
        where: { id: req.params.id }
      })

      if (currentChosen == null) {
        return res.status(404).json({ error: 'class not found' })
      }

      if (currentUser.id != currentChosen.id_user) {
        return res.status(403).json({ error: 'access denied to delete this chosen class' })
      }

      if (currentUser == null) {
        return res.status(403).json({ error: 'access denied to delete this chosen class' })
      }

      await ChosenClass.destroy({
        where: { id: req.params.id }
      });
      res.status(200).json({ message: " The chosen class was deleted!" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error Delete Chosen class "] });
    }
  }

  static async leaveClass(req, res, next) {
    try {
      const token = req.headers.authorization.replace('Bearer ', '');
      const currentUser = await User.findOne({
        where: { token }
      });

      req.body.id_user = currentUser.id;

      const getRoomClass = await RoomClass.findOne({
        where: { id: req.body.id_room_class }
      })

      const newCountJoined = Math.max(getRoomClass.dataValues.countJoined - 1, 0)

      await RoomClass.update({ countJoined: newCountJoined }, { where: { id: req.body.id_room_class } })

      const leaveRoom = await ChosenClass.destroy({
        where: {
          [Op.and]: [{ id_user: currentUser.id }, { id_room_class: req.body.id_room_class }]
        }
      })

      const getTitle = getRoomClass.dataValues.title;
      const getFullname = currentUser.dataValues.fullname;

      await History.create({
        id_user: req.body.id_user,
        id_room_class: req.body.id_room_class,
        history: `You have left ${getTitle} class`,
      });

      res.status(200).json({ message: 'exit room class', leaveRoom })

    } catch (error) {
      console.log(error);
    }
  }

}
module.exports = ChosenClassController;