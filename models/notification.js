'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Notification.belongsTo(models.User, {
        foreignKey: 'id_user'
      })
    }
  };
  Notification.init({
    id_user: DataTypes.INTEGER,
    reminder: DataTypes.STRING,
    dateTime: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'Notification',
  });
  return Notification;
};