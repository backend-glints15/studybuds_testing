'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.History.belongsTo(models.User, {
        foreignKey: 'id_user'
      })
      models.History.belongsTo(models.RoomClass, {
        foreignKey: 'id_room_class'
      })
    }
  };
  History.init({
    id_user: DataTypes.INTEGER,
    id_room_class: DataTypes.INTEGER,
    history: DataTypes.STRING,
    dateTime: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'History',
  });
  return History;
};