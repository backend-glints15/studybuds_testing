'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User.hasMany(models.ChosenClass, {
        foreignKey: 'id_user'
      })
      models.User.hasMany(models.Notification, {
        foreignKey: 'id_user'
      })
      models.User.hasMany(models.History, {
        foreignKey: 'id_user'
      })
      models.User.hasMany(models.RoomClass, {
        foreignKey: 'id_user'
      })
      models.User.hasMany(models.VerifyCode, { foreignKey: 'id_user' })
    }
  };
  User.init({
    fullname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    imageUser: DataTypes.STRING,
    bio: DataTypes.STRING,
    token: DataTypes.STRING,
    // userName: DataTypes.STRING,
    // idfacebook: DataTypes.STRING,
    isVerified: DataTypes.BOOLEAN,
    verificationCode: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'User',
  });
  return User;
};