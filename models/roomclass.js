'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomClass extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.RoomClass.hasMany(models.ChosenClass, {
        foreignKey: 'id_room_class'
      })
      models.RoomClass.hasMany(models.History, {
        foreignKey: 'id_room_class'
      })
      models.RoomClass.belongsTo(models.Topic, {
        foreignKey: 'id_topic'
      })
      models.RoomClass.belongsTo(models.User, {
        foreignKey: 'id_user'
      })
    }
  };
  RoomClass.init({
    id_user: DataTypes.INTEGER,
    id_topic: DataTypes.INTEGER,
    imageClass: DataTypes.STRING,
    title: DataTypes.STRING,
    limitParticipant: DataTypes.INTEGER,
    date: DataTypes.STRING,
    startTime: DataTypes.STRING,
    endStart: DataTypes.STRING,
    description: DataTypes.STRING,
    joinRoom: DataTypes.STRING,
    roomStatus: DataTypes.STRING,
    countJoined: DataTypes.INTEGER,
    roomName: DataTypes.STRING
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'RoomClass',
  });
  return RoomClass;
};