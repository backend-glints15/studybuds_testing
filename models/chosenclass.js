'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ChosenClass extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.ChosenClass.belongsTo(models.User, {
        foreignKey: 'id_user'
      })
      models.ChosenClass.belongsTo(models.RoomClass, {
        foreignKey: 'id_room_class'
      })
    }
  };
  ChosenClass.init({
    id_user: DataTypes.INTEGER,
    id_room_class: DataTypes.INTEGER,
    isApproved: DataTypes.BOOLEAN
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'ChosenClass',
  });
  return ChosenClass;
};